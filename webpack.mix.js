let mix = require('laravel-mix');

mix.options({ processCssUrls: false, publicPath: 'public' });
mix.js('assets/js/app.js', 'public/js');
mix.sass('assets/sass/app.scss', 'public/css');
