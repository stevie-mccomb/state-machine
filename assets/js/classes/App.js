class App
{
    constructor()
    {
        App.instance = this;

        this.element = jQuery('<div class="app"></div>');

        jQuery('body').append(this.element);

        this.loopBound = this.loop.bind(this);

        requestAnimationFrame(this.loopBound);
    }

    loop()
    {
        this.update();
        this.render();

        requestAnimationFrame(this.loopBound);
    }

    update()
    {
        //
    }

    render()
    {
        //
    }
}

module.exports = App;
