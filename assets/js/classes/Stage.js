class Stage
{
    constructor()
    {
        Stage.instance = this;

        this.element = jQuery('<canvas class="stage"></canvas>');

        App.instance.element.append(this.element);

        this.context = this.element.get(0).getContext('2d');

        this.resize();

        this.resizeBound = this.resize.bind(this);

        jQuery(window).on('resize', this.resizeBound);
    }

    update()
    {
        //
    }

    render()
    {
        this.context.clearRect(0, 0, this.width, this.height);
    }

    resize()
    {
        this.element.attr({
            width: this.width,
            height: this.height
        });
    }

    get width()
    {
        return this.element.outerWidth();
    }

    get height()
    {
        return this.element.outerHeight();
    }
}

module.exports = Stage;
